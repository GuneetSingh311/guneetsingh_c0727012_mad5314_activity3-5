//
//  SubscribeController.swift
//  ActivityMAD5314
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import WatchConnectivity
class SubscribeController: UIViewController,WCSessionDelegate {

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    var teamA:String!
    var teamB:String!
    var imageAName:String!
    var imageBName:String!
    var Date:String!
    var GameName:String!
    var gameArray:[String]!
    //var games:[String]? = [""]
    var games:[String] = [String]()     // 1. make a new array to store the subscribed games
    var cellValue:Int? = 0
    @IBOutlet weak var teamAimage: UIImageView!
    @IBOutlet weak var teamBimage: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var teamBName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        teamAimage.image = UIImage(named: imageAName)
        teamBimage.image = UIImage(named: imageBName)
        date.text = Date
        teamAName.text = teamA
        teamBName.text = teamB
        GameName = teamA + " VS " + teamB + " ON " + Date
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }

    }
    
    
    
    @IBAction func subscribeButton(_ sender: Any) {
        print("Clicked subscribe button!")
        
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            self.games.append(GameName!)
            let message = ["SubscribedGame": self.games]
            //let message = ["SubscribedGame": "GOT A MESSAGE FROM THE PHONE!"]
            print(self.games)
            
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
           
        }
    }
    
    @IBAction func unsubscribeButton(_ sender: Any) {
        
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
        
            let message = ["UnsubscribedGame": GameName!]
            
            WCSession.default.sendMessage(message, replyHandler: nil)
            
        }
    }
    
   }
