//
//  ViewController.swift
//  GuneetSingh_C0727012_MAD5314_Activity3-5
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
{
  
    
    
    var teamAName:[String] = ["Germany","Spain","Norway","Australia","Brazil"]
    var teamBName:[String] = ["China PR","South Africa","Nigeria","Italy","Jamaica"]
    var teamAImage:[String] = ["germany.png","spain.png","norway.png","australia.png","brazil.png"]
    var teamBImage:[String] = ["china.png","south-africa.png","nigeria.png","italy.png","jamaica.png"]
    var date:[String] = ["08 jun 2019","08 jun 2019","08 jun 2019","09 jun 2019","09 jun 2019"]
    
    // MARK: Table Outlets.
    @IBOutlet weak var gamesTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gamesTable.delegate=self
        gamesTable.dataSource=self
        gamesTable.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return teamAName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "gamesCell", for: indexPath) as! customCell
        cell.teamAName.text = self.teamAName[indexPath.row]
        cell.teamAImage.image = UIImage(named: self.teamAImage[indexPath.row])
        cell.vsLabel.text = "VS"
        cell.dateLabel.text = self.date[indexPath.row]
        cell.teamBName.text = self.teamBName[indexPath.row]
        cell.teamBImage.image = UIImage(named: self.teamBImage[indexPath.row])
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
  var index = 0
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath.row
        performSegue(withIdentifier: "subController", sender: nil)
        
}
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let vc = segue.destination as! SubscribeController
        vc.imageAName = self.teamAImage[index]
        vc.imageBName = self.teamBImage[index]
        vc.Date = self.date[index]
        vc.teamA = self.teamAName[index]
        vc.teamB = self.teamBName[index]
        vc.cellValue = self.index
    
    }
    
}


