//
//  customCell.swift
//  GuneetSingh_C0727012_MAD5314_Activity3-5
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import UIKit
class customCell: UITableViewCell {
    
    
    
    var value = false
    // MARK: Cell outlets.
    @IBOutlet weak var teamAImage: UIImageView!
    @IBOutlet weak var teamAName: UILabel!
    @IBOutlet weak var vsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var teamBImage: UIImageView!
    @IBOutlet weak var teamBName: UILabel!
}

