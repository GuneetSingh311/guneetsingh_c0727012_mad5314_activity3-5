//
//  rowController.swift
//  ActivityMAD5314 WatchKit Extension
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit

class rowController: NSObject {

    @IBOutlet weak var teamAImage: WKInterfaceImage!
    @IBOutlet weak var teamAName: WKInterfaceLabel!
    @IBOutlet weak var vsLabel: WKInterfaceLabel!
    @IBOutlet weak var date: WKInterfaceLabel!
    @IBOutlet weak var teamBLabel: WKInterfaceImage!
    @IBOutlet weak var teamBName: WKInterfaceLabel!
    
}
