//
//  scheduleGames.swift
//  ActivityMAD5314 WatchKit Extension
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit
import Foundation


class scheduleGames: WKInterfaceController {
  
    var gamesList = [Games]()
    @IBOutlet weak var scheduleGamesTable: WKInterfaceTable!
    // MARK: Variables
    var teamAName:[String] = ["Germany","Spain","Norway","Australia","Brazil"]
    var teamBName:[String] = ["China PR","South Africa","Nigeria","Italy","Jamaica"]
    var teamAImage:[String] = ["germany.png","spain.png","norway.png","australia.png","brazil.png"]
    var teamBImage:[String] = ["china.png","south-africa.png","nigeria.png","italy.png","jamaica.png"]
    var date:[String] = ["08 jun 2019","08 jun 2019","08 jun 2019","09 jun 2019","09 jun 2019"]
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    func createGames() {
        let date1 = "08 june 2019"
        let date2 = "09 june 2019"
     
        let game1 = Games(teamAName: "Germany", teamBName: "ChinaPr",date:date1,imageA:"germany.png",imageB:"china.png")
        let game2 = Games(teamAName: "Spain", teamBName: "SouthAfrica",date:date1,imageA:"spain.png",imageB:"south-africa.png")
        let game3 = Games(teamAName: "Norway", teamBName: "Nigeria",date:date1,imageA:"norway.png",imageB:"nigeria.png")
        let game4 = Games(teamAName: "Australia", teamBName: "Italy",date:date2,imageA:"australia.png",imageB:"italy")
        let game5 = Games(teamAName: "Brazil", teamBName: "Jamaica",date:date2,imageA:"brazil.png",imageB:"jamaica.png")
        gamesList.append(game1)
        gamesList.append(game2)
        gamesList.append(game3)
        gamesList.append(game4)
        gamesList.append(game5)
        
        }
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        self.createGames()
        super.willActivate()
        self.scheduleGamesTable.setNumberOfRows(self.teamAName.count, withRowType: "myRow")
        for(i,games) in self.gamesList.enumerated() {
            let row = self.scheduleGamesTable.rowController(at: i) as! rowController
            row.teamAName.setText(games.teamA)
            row.teamBName.setText(games.teamB)
            row.date.setText(games.date)
            row.teamAImage.setImage(UIImage(named: games.imageA!))
            row.teamBLabel.setImage(UIImage(named: games.imageB!))
}
        
        
        
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
