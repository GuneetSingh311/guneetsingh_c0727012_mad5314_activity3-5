//
//  SubscribedGames.swift
//  ActivityMAD5314 WatchKit Extension
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class SubscribedGames: WKInterfaceController {
    
    
 
    var gameList = arr

    @IBOutlet weak var subgameTable: WKInterfaceTable!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        //self.createGame()
        super.willActivate()
        
        self.subgameTable.setNumberOfRows(self.gameList.count, withRowType: "myRow2")
        for(i,games) in self.gameList.enumerated() {
             let row = self.subgameTable.rowController(at: i) as! rowController2
             row.gameNameLabel.setText(games)
        }
    }
        
   


    override func didDeactivate() {
        // This met!hod is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    


}
