//
//  Games.swift
//  ActivityMAD5314 WatchKit Extension
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import Foundation

class Games: NSObject {
    var teamA:String?
    var teamB:String?
    var date:String?
    var imageA:String?
    var imageB:String?
    // MARK: constuctor
    
    convenience override init() {
    
        self.init(teamAName: "", teamBName: "", date: "",imageA:"",imageB:"")
    }
    init(teamAName:String,teamBName:String,date:String,imageA:String,imageB:String)
    {
        self.teamA = teamAName
        self.teamB = teamBName
        self.date = date
        self.imageA = imageA
        self.imageB = imageB
        
}


}
