//
//  InterfaceController.swift
//  ActivityMAD5314 WatchKit Extension
//
//  Created by Guneet on 2019-03-09.
//  Copyright © 2019 Guneet. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity 
var arr:[String] = [String]()
class InterfaceController: WKInterfaceController,WCSessionDelegate  {
   
   
    var message:[String]?
  
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }


    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
            print("WATCH  - Activated a session!")
        }
    

   }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    
    // WKSession function used by our app
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
      
        
        // output a debug message to the terminal
        print("WATCH - Got a message!")


        var userWantsToUnsubscribe = false
        var unsubscribeGameName = ""
        if (message["UnsubscribedGame"] != nil)
        {
            // person is trying to unsubscribe to a game
            let m = message["UnsubscribedGame"] as! String
            unsubscribeGameName = m
            userWantsToUnsubscribe = true
        }
        
        if (userWantsToUnsubscribe == true) {
            //1 . search the arr array for the game name
            
            // 2. if you find the game name, remove it from the arr array
            for i in 0...arr.count-1 {
                if (arr[i] ==  unsubscribeGameName)
                {
                    print("WATCH - Found the game: \(unsubscribeGameName), index = \(i)")
                    arr.remove(at: i)
                    print("WATCH - REVMOED THE GAME")
                    break
                }
            }
            print("WATCH = updated array: \(arr)")
        }
        else {
            self.message = message["SubscribedGame"] as? [String]
            print(self.message)
            
            if ((self.message?.isEmpty)!) {
                print("User did not subscribe to any games")
                arr = [String]()            // remove everything from the watch's game array
                return
            }
            
            for i in 0...self.message!.count-1
            {
                arr.append(self.message![i])
            }
        }
        

        print("WATCH - final array to use in the watch table view")
        print(arr)
   
    }

    
}
